package components;

import java.util.ArrayList;

public interface Dijkstra_I {
	ArrayList<Vertex_I> path(Vertex_I v1, Vertex_I v2);

	int distance(Vertex_I v1, Vertex_I v2);
}
