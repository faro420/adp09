package components;

public interface Graph_I {
	boolean add(Vertex_I v);

	boolean connect(Vertex_I v1, Vertex_I v2, int distance);

	boolean contains(Vertex_I v);

	boolean remove(Vertex_I v);

	int order();
}
