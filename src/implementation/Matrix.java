package implementation;

import java.util.ArrayList;

import components.Dijkstra_I;
import components.Graph_I;
import components.Vertex_I;

public class Matrix implements Graph_I, Dijkstra_I {
	private Edge[][] matrix;
	private ArrayList<Vertex_I> list;
	private int numberOfEdges;

	public Matrix(int size) {
		matrix = new Edge[size][size];
		list = new ArrayList<Vertex_I>();
	}

	public boolean add(Vertex_I v) {
		if (!contains(v))
			return list.add(v);
		return false;
	}

	public boolean connect(Vertex_I v1, Vertex_I v2, int distance) {
		if (matrix[v1.getID()][v2.getID()] == null) {
			Edge edge = new Edge(v1, v2, distance);
			matrix[v1.getID()][v2.getID()] = edge;
			numberOfEdges++;
			return true;
		}
		return false;
	}

	public boolean contains(Vertex_I v) {
		return list.contains(v);
	}

	public boolean remove(Vertex_I v) {
		for (int i = 0; i < matrix.length; i++) {
			if (matrix[v.getID()][i] != null) {
				matrix[v.getID()][i] = null;
				numberOfEdges--;
			}
		}
		return list.remove(v);
	}

	public int order() {
		return list.size();
	}

	public int size() {
		return numberOfEdges;
	}

	public ArrayList<Vertex_I> path(Vertex_I v1, Vertex_I v2) {
		// TODO Auto-generated method stub
		return null;
	}

	public int distance(Vertex_I v1, Vertex_I v2) {
		// TODO Auto-generated method stub
		return 0;
	}

}
