package implementation;

import java.util.ArrayList;

import components.Dijkstra_I;
import components.Graph_I;
import components.Vertex_I;

public class Graph implements Graph_I, Dijkstra_I {
	private Graph_I type;

	public Graph() {
		type = new List();
	}

	public Graph(int size) {
		type = new Matrix(size);
	}

	public boolean add(Vertex_I v) {
		return type.add(v);
	}

	public boolean connect(Vertex_I v1, Vertex_I v2, int distance) {
		return type.connect(v1, v2, distance);
	}

	public boolean contains(Vertex_I v) {
		return type.contains(v);
	}

	public boolean remove(Vertex_I v) {
		return type.remove(v);
	}

	public int order() {
		return type.order();
	}

	public ArrayList<Vertex_I> path(Vertex_I v1, Vertex_I v2) {
		// TODO Auto-generated method stub
		return null;
	}

	public int distance(Vertex_I v1, Vertex_I v2) {
		// TODO Auto-generated method stub
		return 0;
	}

}
