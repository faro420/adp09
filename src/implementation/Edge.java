package implementation;

import components.Vertex_I;

public class Edge implements Comparable<Edge> {
	private Vertex_I v1, v2;
	private int weight;

	public Edge(Vertex_I v1, Vertex_I v2, int distance) {
		this.v1 = v1;
		this.v2 = v2;
		this.weight = distance;
	}

	public Vertex_I getV1() {
		return v1;
	}

	public Vertex_I getV2() {
		return v2;
	}

	public int getWeight() {
		return weight;
	}

	public void setDistance(int n) {
		weight = n;
	}

	public boolean equals(Edge other) {
		boolean test1 = v1.equals(other.getV1()) && v2.equals(other.getV2());
		boolean test2 = v1.equals(other.getV2()) && v2.equals(other.getV1());
		if ((test1 || test2) && weight == other.getWeight())
			return true;
		return false;
	}

	public boolean contains(Vertex_I v) {
		return v.equals(v1) || v.equals(v2);
	}

	public int compareTo(Edge other) {
		return weight - other.getWeight();
	}

}
