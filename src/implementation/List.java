package implementation;

import java.util.ArrayList;

import components.Dijkstra_I;
import components.Graph_I;
import components.Vertex_I;

public class List implements Graph_I, Dijkstra_I {
	private ArrayList<Vertex_I> list;
	
	public List(){
		list = new ArrayList<Vertex_I>();
	}

	public boolean add(Vertex_I v) {
		if (!contains(v))
			return list.add(v);
		return false;
	}

	public boolean connect(Vertex_I v1, Vertex_I v2, int distance) {
		Edge edge = new Edge(v1, v2, distance);
		ArrayList<Edge> list1 = ((Vlist) v1).getEdges();
		ArrayList<Edge> list2 = ((Vlist) v1).getEdges();
		if (!list1.contains(edge)) {
			list1.add(edge);
			list2.add(edge);
			return true;
		}
		return false;
	}

	public boolean contains(Vertex_I v) {
		return list.contains(v);
	}

	public boolean remove(Vertex_I v) {
		if (list.contains(v)) {
			int n = list.size();
			int m = 0;
			ArrayList<Edge> puffer;
			for (int i = 0; i < n; i++) {
				puffer = ((Vlist) list.get(i)).getEdges();
				m = puffer.size();
				for (int j = 0; j < m; j++) {
					if (puffer.get(j).contains(v)) {
						puffer.remove(j);
						j = m;
					}
				}
			}
			return list.remove(v);
		}
		return false;
	}

	public int order() {
		return list.size();
	}

	public ArrayList<Vertex_I> path(Vertex_I v1, Vertex_I v2) {
		// TODO Auto-generated method stub
		return null;
	}

	public int distance(Vertex_I v1, Vertex_I v2) {
		// TODO Auto-generated method stub
		return 0;
	}

}
