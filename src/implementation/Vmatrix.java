package implementation;

import components.Vertex_I;

public class Vmatrix implements Vertex_I {
	private static int ID = 0;
	private final int id;
	private int minDistance;

	public Vmatrix() {
		id = ID++;
		minDistance = Integer.MAX_VALUE;
	}

	public int minDistance() {
		return minDistance;
	}

	public int getID() {
		return id;
	}

	public boolean equals(Vertex_I other) {
		return id == other.getID();
	}
}
