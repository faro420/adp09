package implementation;

import java.util.ArrayList;

import components.Dijkstra_I;
import components.Vertex_I;

public class Dijkstra implements Dijkstra_I {
	private Dijkstra_I type;

	public Dijkstra(Edge[][] edges, ArrayList<Vertex_I> list) {
		type = new Dijkstra_Matrix(edges, list);
	}

	public Dijkstra(ArrayList<Vertex_I> list) {
		type = new Dijkstra_List(list);
	}

	public ArrayList<Vertex_I> path(Vertex_I v1, Vertex_I v2) {
		return type.path(v1, v2);
	}

	public int distance(Vertex_I v1, Vertex_I v2) {
		return type.distance(v1, v2);
	}

}
