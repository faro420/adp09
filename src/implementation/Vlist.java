package implementation;

import java.util.ArrayList;

import components.Vertex_I;

public class Vlist implements Vertex_I {
	private static int ID = 0;
	private final int id;
	private int minDistance;
	ArrayList<Edge> edges;

	public Vlist() {
		id = ID++;
		minDistance = Integer.MAX_VALUE;
		edges = new ArrayList<Edge>();
	}

	public int minDistance() {
		return minDistance;
	}

	public int getID() {
		return id;
	}

	public ArrayList<Edge> getEdges() {
		return edges;
	}

	public boolean equals(Vertex_I other) {
		return id == other.getID();
	}
}
